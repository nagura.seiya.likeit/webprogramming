<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ削除</title>
    <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
</head>
<body>
    <header style="background-color:dimgray">

    <div class="col row">
        <div class="col-9">
        </div>
            <div class="col-2">
            <a style="color: white">${userInfo.name }さん</a>
            </div>
        <a href="LogoutServlet" style="color: red">
            ログアウト
        </a>
        </div>

</header>

    <h1 style="text-align: center">ユーザ削除確認</h1>


    <div class="col row">
        <div class="col-2">
        </div>
      <div class="col">
          ログインID: ${user.loginId}
      </div>
    </div>

    <div class="col row">
        <div class="col-2">
        </div>
      <div class="col">
          を本当に削除してよろしいでしょうか。
      </div>
    </div>

    <br>
    <br>
    <br>

<div class="container">
  <div class="row">
    <div class="col">
    </div>
    <div class="col">
    <button type="button" style="width: 150px;height: 40px"
            onclick="location.href='UserListServlet'">
            キャンセル
    </button>
    </div>
    <div class="col">
    <form action="userDeleteServlet" method="post">
        <input type="hidden" name="id" value="${user.id}">
        <input type="submit" style="width: 150px;height: 40px" value="OK">
        </form>
    </div>
  </div>
</div>

</body>
</html>