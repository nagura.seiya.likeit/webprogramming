<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ情報詳細参照</title>
    <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
</head>
<body>
	<header style="background-color: dimgray">

		<div class="col row">
			<div class="col-9"></div>
			<div class="col-2">
				<a style="color: white">${userInfo.name }さん</a>
			</div>
			<a href="LogoutServlet" style="color: red"> ログアウト </a>
		</div>

	</header>

    <h1 style="text-align: center">ユーザ情報詳細参照</h1>

    <br>
    <br>


    <div class="col row">
        <div class="col-4">
        </div>
      <div class="col-2">ログインID</div>
      <div class="col">${user.loginId}</div>
    </div>

    <br>
    <div class="col row">
        <div class="col-4">
        </div>
      <div class="col-2">ユーザ名</div>
      <div class="col">${user.name}</div>
    </div>

    <br>
    <div class="col row">
        <div class="col-4">
        </div>
      <div class="col-2">生年月日</div>
        <div class="col">${user.birthDate}</div>
    </div>


    <br>
    <div class="col row">
        <div class="col-4">
        </div>
      <div class="col-2">登録日時</div>
      <div class="col">${user.createDate}</div>
    </div>

    <br>
    <div class="col row">
        <div class="col-4">
        </div>
      <div class="col-2">更新日時</div>
      <div class="col">${user.updateDate}</div>
    </div>

    <br>
    <br>
    <br>
    <br>
 <div class="col row">
     <div class="col">
    <a href="UserListServlet">戻る</a>
     </div>
 </div>
</body>
</html>