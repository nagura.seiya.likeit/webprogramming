<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ログイン画面</title>
    <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
</head>
	<body>

	<br>
		<div class="container">
      		<div class="col">
            <h1 style="text-align: center">ログイン画面</h1>

        <br>
        <br>

      		</div>
        </div>

        <br>
        <br>
    <!-- エラーメッセージの処理 -->
    <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

   <!-- フォーム -->
   <form action="Login" method="post">

    <div class="col row">
        <div class="col-4">
        </div>
      <div class="col-2">ログインID</div>
      <div class="col">
        <input type="text" name="loginId">
      </div>
    </div>

    <br>
    <div class="col row">
        <div class="col-4">
        </div>
      <div class="col-2">パスワード</div>
      <div class="col">
        <input type="password" name="password">
      </div>
    </div>

    <br>
    <div class="container">
      <div class="row">
        <div class="col-5">
        </div>
        <div class="col">
    <input type="submit" style="width: 150px;height: 40px"value="ログイン">
        </div>
      </div>
    </div>
</form>
	</body>
</html>
