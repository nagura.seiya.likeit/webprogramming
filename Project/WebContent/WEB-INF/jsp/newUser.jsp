<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規登録</title>
    <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
</head>
<body>
    <header style="background-color:dimgray">

    <div class="col row">
        <div class="col-9">
        </div>
            <div class="col-2">
            <a style="color: white">${userInfo.name }さん</a>
            </div>
        <a href="LogoutServlet" style="color: red">
            ログアウト
        </a>
        </div>

</header>

    <br>
    <h1 style="text-align: center">ユーザ新規登録</h1>

 <!-- エラーメッセージの処理 -->
    <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
<!-- フォーム -->
    <form action="newUserServlet" method="post">
    <div class="col row">
        <div class="col-4">
        </div>
      <div class="col-2">ログインID</div>
      <div class="col">
        <input type="text" name="loginId">
      </div>
    </div>

    <br>
    <div class="col row">
        <div class="col-4">
        </div>
      <div class="col-2">パスワード</div>
      <div class="col">
        <input type="password" name="password">
      </div>
    </div>

    <br>
    <div class="col row">
        <div class="col-4">
        </div>
      <div class="col-2">パスワード(確認)</div>
      <div class="col">
        <input type="password" name="checkPassword">
      </div>
    </div>

    <br>
    <div class="col row">
        <div class="col-4">
        </div>
      <div class="col-2">ユーザ名</div>
      <div class="col">
        <input type="text" name="name">
      </div>
    </div>

    <br>
    <div class="col row">
        <div class="col-4">
        </div>
      <div class="col-2">生年月日</div>
        <div class="col">
      <input type="date" name="birthDate">
        </div>
    </div>
    <br>

<div class="container">
  <div class="row">
    <div class="col-5">
    </div>
    <div class="col">
    <input type="submit" style="width: 150px;height: 40px" value="登録">
    </div>
  </div>
</div>

</form>
    <br>
    <br>
    <br>
 <div class="col row">
     <div class="col">
    <a href="UserListServlet">戻る</a>
     </div>
 </div>
</body>
</html>