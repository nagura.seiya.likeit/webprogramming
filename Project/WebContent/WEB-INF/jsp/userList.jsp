<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>


	<header style="background-color: dimgray">

		<div class="col row">
			<div class="col-9"></div>
			<div class="col-2">
				<a style="color: white">${userInfo.name }さん</a>
			</div>
			<a href="LogoutServlet" style="color: red"> ログアウト </a>
		</div>

	</header>



	<h1 style="text-align: center">ユーザ一覧</h1>

	<div class="col row">
		<div class="col-11"></div>
		<a href="newUserServlet">新規登録</a>
	</div>

    <!-- 検索フォーム -->
<form action="UserListServlet" method="post">
	<div class="container">
	 <div class="row">
	 <div class="col-4"></div>
		<div class="col-2">ログインID</div>
		<div class="col">
			<input type="text" name="inputLoginId">
		</div>
		</div>
	</div>

	<br>
	<div class="container">
	 <div class="row">
		<div class="col-4"></div>
		<div class="col-2">ユーザ名</div>
		<div class="col">
			<input type="text" name="inputName">
		</div>
		</div>
	</div>

	<br>
	<div class="container">
	 <div class="row">
		<div class="col-4"></div>
		<div class="col-2">生年月日</div>
		<div class="col">
			<input type="date" name="inputBirthDateS"> ～ <input type="date" name="inputBirthDateE">
		</div>
		</div>
	</div>
	<br>

	<div class="container">
		<div class="row">
		　<div class="col-5"></div>
			<div class="col">
				<input type="submit" value="検索" style="width: 140px; height: 35px">
			</div>
		</div>
	</div>
</form>

<br>
    <!-- SQLデータのテーブル -->
	<div class="table-responsive">
             <table class="table table-striped">
               <thead>
                 <tr>
                   <th>ログインID</th>
                   <th>ユーザ名</th>
                   <th>生年月日</th>
                   <th></th>
                 </tr>
               </thead>
          <!-- ログイン時に取得したデータを画面に出力 -->
               <tbody>
                 <c:forEach var="user" items="${userList}">
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>

                     <td>
                     <c:if test="${userInfo.loginId == 'admin'}" var="flg" />

                     <!-- 管理者ならば全ての操作が可能 -->
                     <c:if test="${flg}" >
                     <a class="btn btn-primary" href="userDetailServlet?id=${user.id}">詳細</a>
                     <a class="btn btn-success" href="userUpdateServlet?id=${user.id}">更新</a>
                     <a class="btn btn-danger" href ="userDeleteServlet?id=${user.id}">削除</a>
                     </c:if>

                      <!-- そうではない場合は詳細閲覧および自身のデータの更新のみ -->
                     <c:if test="${!flg}" >
                     <a class="btn btn-primary" href="userDetailServlet?id=${user.id}">詳細</a>
                     <c:if test="${userInfo.loginId == user.loginId}">
                     <a class="btn btn-success" href="userUpdateServlet?id=${user.id}">更新</a>
                     </c:if>
                     </c:if>
                     </td>
                   </tr>
                 </c:forEach>
               </tbody>
             </table>
           </div>

</body>
</html>