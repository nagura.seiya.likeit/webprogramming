package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import Beans.User;

public class UserDao {

	public User findByLoginInfo(String loginId, String password) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, loginId);
			stmt.setString(2, Encryption(password));

			ResultSet rs = stmt.executeQuery();

			//ログイン失敗時
			if (!rs.next()) {
				return null;
			}

			//ログイン成功時
			String loginidData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginidData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	/**全てのユーザ情報を取得**/
	public List<User> findAll() {

		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "SELECT * FROM user WHERE id != 1";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {

				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return userList;

	}

	/**テーブルにデータを追加**/
	public User InsertUser(String loginId, String name, String birthDate, String password) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "INSERT INTO user (login_id,name,birth_date,password,create_date,update_date) VALUES (?,?,?,?,now(),now())";

			stmt = conn.prepareStatement(sql);

			stmt.setString(1, loginId);
			stmt.setString(2, name);
			stmt.setString(3, birthDate);
			stmt.setString(4, Encryption(password));

			//SQLの実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**対象のidが持ってるデータの取得**/
	public User findDetail(String id) {

		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "SELECT * FROM user WHERE id = ?";

			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, id);

			ResultSet rs = stmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int id2 = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			User user = new User(id2, loginId, name, birthDate, password, createDate, updateDate);

			return user;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;

	}

	/**テーブルデータの更新**/
	public User Update(String name, String birthDate, String password, String id) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "UPDATE user SET name= ?,birth_Date= ?,password= ?,update_Date= now() WHERE id =?";

			stmt = conn.prepareStatement(sql);

			stmt.setString(1, name);
			stmt.setString(2, birthDate);
			stmt.setString(3, Encryption(password));
			stmt.setString(4, id);

			//SQLの実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**対象のidデータの削除**/
	public User Delete(String id) {

		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "DELETE FROM user WHERE id = ?";

			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, id);

			//SQLの実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;

	}

	/**検索処理**/
	public List<User> findUser(String inputLoginId, String inputName, String inputBirthDateS,String inputBirthDateE) {

		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "SELECT * FROM user WHERE id != 1";

			if(!inputLoginId.equals("")) {
				sql += " AND login_id = '" + inputLoginId + "'";
			}

			if(!inputName.equals("")) {
				sql += " AND name LIKE '%" + inputName + "%'";
			}

			if(!inputBirthDateS.equals("")) {
				sql += " AND birth_date >'" + inputBirthDateS + "'";
			}

			if(!inputBirthDateE.equals("")) {
				sql += " AND birth_date <'" + inputBirthDateE + "'";
			}

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {

				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return userList;

	}
//	public List<User> findUser(String Inputname) {
//
//		Connection conn = null;
//		List<User> userList = new ArrayList<User>();
//		try {
//			// データベースへ接続
//			conn = DBManager.getConnection();
//
//			// TODO ここに処理を書いていく
//			String sql = "SELECT * FROM user WHERE name LIKE ?";
//
//			PreparedStatement stmt = conn.prepareStatement(sql);
//			stmt.setString(1, "%" + Inputname + "%");
//			ResultSet rs = stmt.executeQuery();
//
//			while (rs.next()) {
//
//				int id = rs.getInt("id");
//				String loginId = rs.getString("login_id");
//				String name = rs.getString("name");
//				Date birthDate = rs.getDate("birth_date");
//				String password = rs.getString("password");
//				String createDate = rs.getString("create_date");
//				String updateDate = rs.getString("update_date");
//				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);
//
//				userList.add(user);
//			}
//
//		} catch (SQLException e) {
//			e.printStackTrace();
//		} finally {
//			// データベース切断
//			if (conn != null) {
//				try {
//					conn.close();
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}
//			}
//		}
//		return userList;
//
//	}

	/**暗号化の処理**/
	public String Encryption(String password) {
		//ハッシュを生成したい元の文字列
		String source = "password";

		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;

		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);
		return result;
	}
}
